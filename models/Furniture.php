<?php

namespace app\models;

class Furniture extends BaseModel
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ],
        ];
    }

    public static function label(){
        return 'Мебель и аксессуары';
    }


    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'description' => 'Описание',
            'discount_cost' => 'Цена по скидке',
            'total_cost' => 'Цена',
            'image' => 'Изображения',
        ];
    }

    public static function tableName(){
        return '{{%furniture}}';
    }
}
