<?php

use yii\db\Migration;

class m161026_163503_create_tables extends Migration
{

    const TABLE_INTERIORS = 'tbl_interiors';
    const TABLE_PRODUCTS = 'tbl_products';

    public function safeUp()
    {
        $this->createTable(self::TABLE_INTERIORS, [
            'id' => 'pk',
            'name' => 'varchar(255)',
            'alias' => 'varchar(25)',
            'description' => 'longtext',
            'total_cost' => 'int(11)',
            'discount_cost' => 'int(11)',
            'square' => 'int(11)',
            'projects_cost' => 'int(11)',
            'renovations_cost' => 'int(11)',
            'furniture_cost' => 'int(11)',
            'decorations_cost' => 'int(11)',
        ]);

        $this->createTable(self::TABLE_PRODUCTS, [
            'id' => 'pk',
            'available' => 'int(1) DEFAULT 1',
            'name' => 'varchar(255)',
            'alias' => 'varchar(25)',
            'description' => 'varchar(255)',
            'total_cost' => 'int(11)',
            'discount_cost' => 'int(11)',
            'qtn' => 'int(11)',
            'article' => 'varchar(45)',
            'style_type' => 'varchar(255)',
        ]);

    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_INTERIORS);
        $this->dropTable(self::TABLE_PRODUCTS);
    }
}
