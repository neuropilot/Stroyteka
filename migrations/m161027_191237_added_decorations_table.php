<?php

use yii\db\Migration;

class m161027_191237_added_decorations_table extends Migration
{

    const TABLE_DECORATIONS = 'tbl_decorations';
    const TABLE_RELATIONS = 'tbl_interiors_decorations';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE_DECORATIONS, [
            'id' => 'pk',
            'name' => 'varchar(45)',
            'description' => 'text',
            'discount_cost' => 'int(11)',
            'total_cost' => 'int(11)',
        ]);

        $this->createTable(self::TABLE_RELATIONS, [
            'id' => 'pk',
            'interior_id' => 'int(11)',
            'decorations_id' => 'int(11)',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_DECORATIONS);
        $this->dropTable(self::TABLE_RELATIONS);
    }
}
