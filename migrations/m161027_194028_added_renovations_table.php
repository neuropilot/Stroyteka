<?php

use yii\db\Migration;

class m161027_194028_added_renovations_table extends Migration
{
    const TABLE_RENOVATIONS = 'tbl_renovations';
    const TABLE_RELATIONS = 'tbl_interiors_renovations';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE_RENOVATIONS, [
            'id' => 'pk',
            'name' => 'varchar(45)',
            'description' => 'text',
            'discount_cost' => 'int(11)',
            'total_cost' => 'int(11)',
        ]);

        $this->createTable(self::TABLE_RELATIONS, [
            'id' => 'pk',
            'interior_id' => 'int(11)',
            'renovations_id' => 'int(11)',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_RENOVATIONS);
        $this->dropTable(self::TABLE_RELATIONS);
    }
}
