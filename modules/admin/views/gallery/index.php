<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Галерея интерьеров';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page">
    <br><br>
    <div class="container">
        <div class="gallery-index">

            <h1><?= Html::encode($this->title) ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <br>
            <p>
                <?= Html::a('Добавить проект', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'title',
                    'totalcost',
                    'square',
                    'projectcost',
                    // 'repaircost',
                    // 'finishcost',
                    // 'furniturecost',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
    <br><br>
</section>
