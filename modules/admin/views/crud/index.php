<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helpers\Helper;


$this->title = $searchModel::label();
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page">
    <br><br>
    <div class="container">
        <div class="gallery-index">

            <h1><?= Html::encode($this->title) ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <br>
            <p>
                <?= Html::a('Добавить проект', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => Yii::$app->controller->compareIndexColumns($attributes),
            ]); ?>
        </div>
    </div>
    <br><br>
</section>
