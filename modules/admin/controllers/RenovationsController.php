<?php
namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Renovations;
use app\components\BaseController;
use yii\web\NotFoundHttpException;

class RenovationsController extends BaseController {

    public function getIndexAttributes(){
        return [
            'name',
            'description',
            'total_cost',
        ];
    }

}
