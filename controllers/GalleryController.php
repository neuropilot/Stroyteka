<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\modules\admin\models\Gallery;

class GalleryController extends Controller
{
    public $layout = 'gallery';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => [
                            'index','view'
                        ],
                        'allow' => true,
                        // 'roles' => ['@'],
                    ],
                ],
            ],

        ];
    }


    public function actionIndex()
    {
        $gallery = Gallery::find()->all();
        return $this->render('index',[
            'gallery' => $gallery
        ]);
    }

    public function actionView()
    {
        $item = Gallery::find()->where(['id'=>Yii::$app->getRequest()->getQueryParam('id')])->one();

        return $this->render('view', [
            'item' => $item,
        ]);
    }

}
