<?php

namespace app\controllers;

use Yii;
use app\components\BaseController;
use app\models\Products;

class ProductsController extends BaseController
{
    public function actionIndex($categoryName = false){
        $products = Products::find()->all();

        return $this->render('index',[
            'products' => $products,
        ]);
    }

    public function actionView($alias)
    {
        $item = Products::findByAlias($alias);

        return $this->render('view', [
            'item' => $item,
        ]);
    }
}
