$(function() {
	//E-mail Ajax Send
	//Documentation & Example: https://github.com/agragregra/uniMail
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Thank you!");
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});
	
	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

    // Menu
	$('.toggle-button').click(function(){
          $('.menu-wrapper').toggleClass('menu-wrapper-toggle-none');
          $('.open-menu').toggleClass('open-menu-open');
    });
    $('.toAnchor').on('click', function () {
        $('.menu-wrapper').removeClass('menu-wrapper-toggle-none');
    });

	$('.accordion-button').click(function(){
		$(this).toggleClass('collapsed-in');
	});

    // Плавный скрол 
   	$('.toAnchor').on('click', function () {
       	$('.navbar-collapse').removeClass('in');
           $a = $($(this).attr('href'));
           $('html,body').animate({ scrollTop: $a.offset().top - 110}, 500);
        return false;
    });

    // Маска для телефона
    jQuery(function($){
        $('.phone').mask('+7 (999) 999-9999');
    });

	// Modal
	$('.button-consult').magnificPopup({
		showCloseBtn:true,
		closeOnBgClick:true
	});

	$('.call-button').magnificPopup({
		showCloseBtn:true,
		closeBtnInside:true,
		closeOnBgClick:true
	});

	$('.buy_btn').magnificPopup({
		showCloseBtn:true,
		closeOnBgClick:true,
		mainClass: 'mfp-white'
	});

	// Hover 
	$('.go-catalog1').hover(function(){
        $('.ab-href1').toggleClass('bg-href');
    });
    $('.go-catalog2').hover(function(){
       $('.ab-href2').toggleClass('bg-href');
    });
    // Install swiper
    var mySwiper = new Swiper ('.swiper-container', {
	    // Optional parameters
	    loop: true,
	    paginationClickable: true,
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: 2500,
        autoplayDisableOnInteraction: false,
       	effect: 'fade',
   		speed: 2000,
	})
});

