<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О нас';
// $this->params['breadcrumbs'][] = $this->title;
?>

<section class="page">
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="/">Главная</a></li>
			<li><a href="/about.html">О нас</a></li>
		</ul>
		<div class="row page--wrapper">
			<div class="col-xs-12 page--wrapper-contact">
				<div class="page--wrapper-contact-title">
					<h1>О нас</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam, asperiores aspernatur 						beatae dolorem eligendi illo in iste laboriosam maiores odit quo reiciendis rerum totam voluptatum? 						Incidunt laboriosam laudantium voluptate!
					</p>
				</div>
				<div class="page--wrapper-about">
					<div class="page--wrapper-about-album">
						<img src="img/about.png" alt="">
					</div>
					<div class="page--wrapper-about-description">
						<div class="page--wrapper-about-description-title">
							<h2>Заголовок второго уровня</h2>
						</div>
						<div class="row page--wrapper-about-content">
							<div class="col-md-6">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae commodi dolorem id
									molestiae pariatur porro, tempora voluptas? Consequatur cum ea fuga libero rerum
									sapiente. Eum necessitatibus nemo porro recusandae veritatis. Lorem ipsum dolor sit
									amet, consectetur adipisicing elit. Deleniti eum fugiat labore laborum sunt
									suscipit, ullam voluptatibus? Ad alias amet commodi eum explicabo facere facilis,
									magnam molestias neque odit, rem.</p>
							</div>
							<div class="col-md-6">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid atque error esse
									expedita facilis fugit harum ipsum, magnam magni nam neque odit officia officiis,
									perspiciatis placeat quam quisquam quod rem. Lorem ipsum dolor sit amet, consectetur
									adipisicing elit. Adipisci alias consectetur consequuntur delectus dicta dolorum
									expedita facilis, in inventore laborum libero molestiae nam nihil porro quasi
									recusandae veniam? A, earum.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>