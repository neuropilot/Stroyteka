<?php

/* @var $this yii\web\View */

$this->title = "Каталог";
?>

<section class="page">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li><a href="/catalog.html">Каталог</a></li>
        </ul>
        <div class="row page--wrapper">
            <div class="col-lg-2 col-md-3 col-xs-12 page--wrapper-left hidden">
                <div class="page--wrapper-left--category">
                    <div class="category--title">Категори</div>
                    <ul class="category--wrapper">
                        <li>
                            <img src="/img/category-icon/icon1.png"  alt="">
                            <a href=""></a>
                            <p>Внутренняя отделка стен</p>
                        </li>
                        <li>
                            <img src="/img/category-icon/icon2.png"  alt="">
                            <a href=""></a>
                            <p>Напольные покрытия</p>
                        </li>
                        <li>
                            <img src="/img/category-icon/icon3.png"  alt="">
                            <a href=""></a>
                            <p>Для ванной</p>
                        </li>
                        <li>
                            <img src="/img/category-icon/icon4.png"  alt="">
                            <a href=""></a>
                            <p>Для гостиной</p>
                        </li>
                        <li>
                            <img src="/img/category-icon/icon5.png"  alt="">
                            <a href=""></a>
                            <p>Для спальни</p>
                        </li>
                        <li>
                            <img src="/img/category-icon/icon6.png"  alt="">
                            <a href=""></a>
                            <p>Для столов</p>
                        </li>
                        <li>
                            <img src="/img/category-icon/icon7.png"  alt="">
                            <a href=""></a>
                            <p>Для офиса</p>
                        </li>
                        <li>
                            <img src="/img/category-icon/icon8.png"  alt="">
                            <a href=""></a>
                            <p>Для детской</p>
                        </li>
                        <li>
                            <img src="/img/category-icon/icon9.png"  alt="">
                            <a href=""></a>
                            <p>Для кухни</p>
                        </li>
                        <li>
                            <img src="/img/category-icon/icon10.png" alt="">
                            <a href=""></a>
                            <p>Свет</p>
                        </li>
                        <li>
                            <img src="/img/category-icon/icon11.png" alt="">
                            <a href=""></a>
                            <p>Двери</p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Было: col-lg-10 col-md-9 col-xs-12 -->
            <div class="col-lg-12 col-md-12 col-xs-12 page--wrapper-right">
                <div class="page--wrapper-right-title">
                    <h1>Каталог</h1>
                </div>
                <div class="page--wrapper-right-description">

                </div>
                <div class="page--wrapper-right-items-catalog">
                    <?php foreach ($products as $item): ?>
                    <article class="catalog-item">
                        <div class="catalog-item--left">
                            <div class="flexslider c_slider">
                                <ul class="slides">
                                    <?php foreach ($item->getImages() as $image): ?>
                                        <li>
                                            <a href="<?= $image->getUrl(); ?>" data-lightbox="roadtrip" class="zoom-photo">
                                                <img src="<?= $image->getUrl(); ?>" />
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="catalog-item--right">
                            <a href="<?= Yii::$app->urlManager->createUrl(['products/view', 'alias' => $item->alias]); ?>"><?= $item->name; ?></a>
                            <div class="tags hidden">
                                <a href="">Совремeнный,</a>
                                <a href="">Модернизм,</a>
                                <a href="">Фьюжн</a>
                            </div>
                            <span class="price"><?= $item->total_cost; ?> тенге</span>
                            <div class="flexslider c_carousel">
                                <ul class="slides">
                                    <li>
                                        <img src="/img/divan.jpg" />
                                    </li>
                                    <li>
                                        <img src="/img/divan.jpg" />
                                    </li>
                                    <li>
                                        <img src="/img/divan.jpg" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </article>
                    <?php endforeach; ?>
                </div>

                <ul class="pagination clearfix hidden">
                    <li><a class="active" href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
