<?php

namespace app\components;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\modules\admin\models\Image;

/**
 * Класс реализует базовый функционал, используемый во многих действиях.
 */
class BaseController extends Controller
{
    /**
     * Получить требуемый экземпляр модели (Models Factory)
     * @param  int  $id            Первичный ключ
     * @param  boolean $modelName  На всякий случай, если имя модели отличается от айди контроллера
     * @return ActiveRecord        Экземпляр модели
     */
    protected function findModel($id, $new = false, $modelName = false) {

        if (!$modelName) {
            $modelName = ucfirst($this->id);
        }

        $namespace = explode('\\', parent::className());

        $namespace = in_array('admin', $namespace) ? "\\app\\modules\\admin\\models\\" : "\\app\\models\\";

        $modelName = $namespace . $modelName;

        if ($model = $new ? new $modelName : $modelName::findOne($id)) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Собирает массив для вывода в GridView
     * @param  array $columns Колонки из аттрибутов модели
     * @return array          Сконфигурированный массив
     */
    public function compareIndexColumns($columns){
        array_unshift($columns, ['class' => 'yii\grid\SerialColumn']);
        array_push($columns, ['class' => 'yii\grid\ActionColumn']);

        return $columns;
    }

    /**
     * Метод сохраняет изображения, если они прилетели в $_POST
     * @param  ActiveRecord $model Обрабатываемая модель
     * @return void
     */
    protected function loadImage($model){
        if (isset($model->image)) {
            $model->image = \yii\web\UploadedFile::getInstances($model, 'image');
            if($model->image) {
                foreach ($model->image as $image) {
                    $path = Yii::getAlias('@webroot/upload/files/').$image->baseName . '.' . $image->extension;
                    $image->saveAs($path);
                    $model->attachImage($path);
                }

            }
        }
    }

    /**
     * Просмотр списка записей в админке
     * @return View
     */
    public function actionIndex(){

        $searchModel = $this->findModel(true, true);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/crud/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'attributes' => $this->getIndexAttributes(),
        ]);
    }

    /**
     * Создание новой записи
     * @return View
     */
    public function actionCreate(){

        $model = $this->findModel(true, true);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->loadImage($model);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('/crud/create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Обновление записи
     * @param  ingeger $id Первичный ключ модели для поиска
     * @return View
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->loadImage($model);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('/crud/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Просмотр конкретной записи
     * @param  integer $id Первичный ключ для поиска модели
     * @return View
     */
    public function actionView($id){

        $model = $this->findModel($id);

        $renderAttributes = [];
        foreach ($model->attributes as $key => $value) {
            $renderAttributes[] = $key;
        }

        /**
         * Если нужно переопределить аттрибуты для вывода, делаем это в методе mixinView
         */
        if (method_exists($this, 'mixinView')) {
            $renderAttributes = call_user_func([$this, 'mixinView'], $model, $renderAttributes);
        }

        return $this->render('/crud/view', [
            'model' => $model,
            'attributes' => $renderAttributes,
        ]);
    }

    /**
     * Удаление записи
     * @param  integer $id Первичный ключ для поиска модели
     * @return View
     */
    public function actionDelete($id){
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Метод для удаления изображения с помощью ajax
     * @param  integer $id Первичный ключ таблицы image для удаления
     * @return void
     */
    public function actionDeleteImage($id){
        if (Yii::$app->request->isAjax) {
            if (Image::findOne($id)->delete()) {
                echo "Image removed";
            }
        }
    }
}
